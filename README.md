# Ashesi 2024 - Routing Project

This is the repository that contains the assignment of the routing project for the 2024 Ashesi block course "Advanced Communication Systems and Internet of Things."

The assignment and documentation are available in the [wiki](https://gitlab.ethz.ch/nsg/public/ashesi-24-routing-project/-/wikis/home).
